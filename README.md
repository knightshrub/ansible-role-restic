Ansible Role: restic
======================

Set up a restic backup service of the user's `$HOME` directory as a systemd
user unit.

Requirements
------------

You should define a file `files/{{ ansible_host }}/restic.exclude` that
defines the files to exclude, see `man restic backup`.

Role Variables
--------------

Available variables are listed below, along with their default values (see `defaults/main.yml`):

Settings for which backups to keep during repository pruning (see `man restic-prune`)
```
restic_keep_within_hourly: 1d
restic_keep_within_daily: 1m
restic_keep_within_weekly: 1y
restic_keep_monthly: unlimited
```

Settings related to the SSH connection to the remote backup host.
```
restic_ssh_key_type: ed25519
restic_ssh_key_file: "{{ ansible_user_dir }}/.ssh/restic_{{ restic_ssh_key_type }}"
restic_remote_port: 22
```

Dependencies
------------

None.

License
-------

BSD
